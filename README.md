# Tizen / WebOS scripts #

Scripts to create packages for Tizen (WGT) and Webos (IPK). These will look for a `build` folder and create a package using the files inside.

You can configure your `bash_profile` file to run them globally.

## Requirements

Tizen CLI, WebOS CLI
